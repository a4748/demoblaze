package co.com.tuya.certification.stepdefinitions;

import co.com.tuya.certification.interactions.NavegarMenu;
import co.com.tuya.certification.questions.ValidarInfoProductos;
import co.com.tuya.certification.questions.ValidarPaginaLaptops;
import co.com.tuya.certification.questions.ValidarPrimeraPaginaLaptops;
import co.com.tuya.certification.questions.ValidarTexto;
import co.com.tuya.certification.tasks.NavegarEntrePagina;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Managed;
import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;
import static co.com.tuya.certification.Constantes.Constantes.*;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class ConsultarCategoriaStepDefinitions {

    @Managed(driver = "chrome")
    WebDriver hisdriver;

    @Before
    public void setTheStage() {
        OnStage.setTheStage(new OnlineCast());
        OnStage.theActorCalled(ACTOR);
    }

    @Dado("^que el usuario se encuentra en la ventana de inicio de la pagina web Product Store$")
    public void queElUsuarioSeEncuentraEnLaVentanaDeInicioDeLaPaginaWebProductStore() {
        theActorInTheSpotlight().wasAbleTo(Open.url(URL));
    }

    @Cuando("^el usuario selecciona el item (.*) del menu lateral$")
    public void elUsuarioSeleccionaElItemDelMenuLateral(String opcion) {
        theActorInTheSpotlight().attemptsTo(NavegarMenu.navegarItems(opcion));
    }

    @Entonces("^el sistema mostrara la pagina de categoria de laptops viendo el (.*) de las laptops$")
    public void elSistemaMostraraLaPaginaDeCategoriaDeLaptopsViendoElDeLasLaptops(String nombre) {
        theActorInTheSpotlight().should(seeThat(ValidarPaginaLaptops.validarPagina(nombre), Matchers.is(nombre)));
    }

    @Entonces("^el sistema muestra los (.*) la (.*) y (.*) de las laptops  disponibles$")
    public void elSistemaMuestraLosLaYDeLasLaptopsDisponibles(String nom, String desc, String precio) {
        String vec[] = new String[2];
        vec[0] = nom.trim();
        vec[1] = precio.trim();

        theActorInTheSpotlight().should(seeThat(ValidarInfoProductos.validar(nom, precio), Matchers.arrayContaining(vec)));
        theActorInTheSpotlight().should(seeThat(ValidarTexto.validarNulo(desc), Matchers.is(true)));
    }

    @Cuando("^el usuario selecciona los iconos para navegar entre productos$")
    public void elUsuarioSeleccionaLosIconosParaNavegarEntreProductos() {
        theActorInTheSpotlight().attemptsTo(NavegarEntrePagina.navegar());
    }


    @Entonces("^el sistema navegara en la paginacion de laptops$")
    public void elSistemaNavegaraEnLaPaginacionDeLaptops() {
        theActorInTheSpotlight().should(seeThat(ValidarPrimeraPaginaLaptops.validarPrimeraPagina(), Matchers.is(true)));
    }

}
