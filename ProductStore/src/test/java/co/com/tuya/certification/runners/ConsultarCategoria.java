package co.com.tuya.certification.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/consultar_categoria_laptops.feature",
        glue = "co.com.tuya.certification.stepdefinitions",
        snippets = SnippetType.CAMELCASE)

public class ConsultarCategoria {
}
