#language: es
#encoding: UTF-8

Característica: Yo como cliente del almacén demoblaze
  Necesito visualizar los precios de
  los productos de la categoria Laptops
  Para tomar decisiones de compra

  Antecedentes:
    Dado que el usuario se encuentra en la ventana de inicio de la pagina web Product Store
    Cuando el usuario selecciona el item laptops del menu lateral

  Esquema del escenario:  El sistema redirige a la pagina de categoria de laptops
    Entonces el sistema mostrara la pagina de categoria de laptops viendo el <nombre> de las laptops
    Ejemplos:
      | nombre       |
      | Sony vaio i5 |

  Esquema del escenario: El sistema muestra la informacion de los productos de laptops
    Entonces el sistema muestra los <nombres>  la <descripcion> y <precio>  de las laptops  disponibles

    Ejemplos:
      | nombres      | descripcion               | precio |
      | Sony vaio i5 | Sony is so confident that | $790   |


Escenario: El sistema navega en la paginacion de los productos de la categoria laptops
    Cuando el usuario selecciona los iconos para navegar entre productos
    Entonces el sistema navegara en la paginacion de laptops



