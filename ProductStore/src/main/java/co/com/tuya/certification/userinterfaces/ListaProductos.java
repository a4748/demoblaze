package co.com.tuya.certification.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class ListaProductos {

    public  static final Target NEXT = Target.the("next")
            .located(By.id("next2"));

    public  static final Target PREVIOUS = Target.the("previous")
            .located(By.id("prev2"));

    public  static final Target PRIMERPRODUCTO = Target.the("primer producto")
            .located(By.xpath("(//div[@class='card h-100']//h4[@class='card-title'])[1]"));

    public static Target nombre(String nombre) {
        return Target.the("Nombre de producto").
                located(By.xpath("//a[contains(text(),'" +nombre.trim()+"')]"));
    }
    public static Target precio(String precio) {
        return Target.the("precio del producto").
                located(By.xpath("//h5[contains(text(),'" + precio.trim() + "')]"));
    }
    public static Target descripcion(String descripcion) {
        return Target.the("descripcion del producto").
                located(By.xpath("//p[contains(text(),'" + descripcion.trim() + "')]"));
    }

    public ListaProductos() {
    }
}
