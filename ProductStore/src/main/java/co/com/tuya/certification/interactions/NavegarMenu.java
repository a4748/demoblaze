package co.com.tuya.certification.interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.tuya.certification.Constantes.Constantes.TIPO_PRODUCTO;
import static co.com.tuya.certification.userinterfaces.MenuLateral.LAPTOPS;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class NavegarMenu implements Interaction {
    String opcion;

    public NavegarMenu(String opcion) {
        this.opcion = opcion;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if (opcion.equals(TIPO_PRODUCTO)) {
            actor.attemptsTo(
                    Click.on(LAPTOPS)
            );
        } else {
        }

    }

    public static NavegarMenu navegarItems(String opcion) {
        return instrumented(NavegarMenu.class, opcion);
    }


}
