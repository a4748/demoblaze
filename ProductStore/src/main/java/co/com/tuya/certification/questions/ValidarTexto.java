package co.com.tuya.certification.questions;

import co.com.tuya.certification.userinterfaces.ListaProductos;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class ValidarTexto implements Question {
    String des;
    public ValidarTexto(String des){
        this.des = des;
    }
    @Override
    public Object answeredBy(Actor actor) {
        return ListaProductos.descripcion(des).resolveFor(actor).isVisible();
    }
    public static  ValidarTexto validarNulo(String des){
        return new ValidarTexto(des);
    }
}
