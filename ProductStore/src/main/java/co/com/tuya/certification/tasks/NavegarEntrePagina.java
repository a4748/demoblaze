package co.com.tuya.certification.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.tuya.certification.Constantes.Constantes.NOMBRES;
import static co.com.tuya.certification.userinterfaces.ListaProductos.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class NavegarEntrePagina implements Task {
    String vect[] = new String[2];

    @Override
    public <T extends Actor> void performAs(T actor) {
        vect[0] = PRIMERPRODUCTO.resolveFor(actor).getText();

        actor.attemptsTo(
                Click.on(NEXT),
                Click.on(PREVIOUS)
        );
        vect[1] = PRIMERPRODUCTO.resolveFor(actor).getText();
      actor.remember(NOMBRES,vect);
    }

    public static NavegarEntrePagina navegar() {
        return instrumented(NavegarEntrePagina.class);
    }
}
