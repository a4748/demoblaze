package co.com.tuya.certification.questions;

import co.com.tuya.certification.userinterfaces.ListaProductos;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class ValidarInfoProductos implements Question {
    String nom;
    String precio;

    public ValidarInfoProductos(String nom, String precio) {
        this.nom = nom;
        this.precio = precio;
    }

    @Override
    public Object answeredBy(Actor actor) {
        String datos[] = new String[2];
        datos[0] = Text.of(ListaProductos.nombre(nom)).viewedBy(actor).asString();
        datos[1] = Text.of(ListaProductos.precio(precio)).viewedBy(actor).asString();
        return datos;

    }

    public static ValidarInfoProductos validar(String nom, String precio) {
        return new ValidarInfoProductos(nom,precio);
    }
}
