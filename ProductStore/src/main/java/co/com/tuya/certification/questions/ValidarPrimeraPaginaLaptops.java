package co.com.tuya.certification.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.tuya.certification.Constantes.Constantes.NOMBRES;

public class ValidarPrimeraPaginaLaptops implements Question {
    String nombre1;
    String nombre2;


    @Override
    public Object answeredBy(Actor actor) {

       String vect[]= actor.recall(NOMBRES);

        if (vect[0].equals(vect[1])) {
            return true;
        } else {
            return false;
        }
    }

    public static ValidarPrimeraPaginaLaptops validarPrimeraPagina() {
        return new ValidarPrimeraPaginaLaptops();
    }
}
