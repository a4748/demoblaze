package co.com.tuya.certification.questions;

import co.com.tuya.certification.userinterfaces.ListaProductos;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class ValidarPaginaLaptops implements Question {
    String nombre;

    public ValidarPaginaLaptops(String nombre){
        this.nombre=nombre;
    }

    @Override
    public Object answeredBy(Actor actor) {
        return Text.of(ListaProductos.nombre(nombre)).viewedBy(actor).asString();
    }

    public static ValidarPaginaLaptops validarPagina(String nombre){
        return  new ValidarPaginaLaptops(nombre);
    }
}
