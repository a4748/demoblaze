package co.com.tuya.certification.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class MenuLateral {
    public static final Target LAPTOPS = Target.the("Item laptops del menu lateral").
            located(By.xpath("//a[contains(text(),'Laptops')]"));

    public MenuLateral() {
    }
}
